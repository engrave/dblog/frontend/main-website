import { Response, NextFunction } from "express";
import * as express from 'express';
import * as os from 'os';

let router = express.Router();

router.get('/__health', (req: any, res: Response, next: NextFunction) => {
    return res.json({
        message: 'pong',
        version: process.env.npm_package_version,
        name: process.env.npm_package_name,
        instance: os.hostname()
    });
});


module.exports = router;