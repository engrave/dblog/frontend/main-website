import getLatestBlogs from './actions/getLatestBlogs';
import getLatestArticles from './actions/getLatestArticles';

const exploreService = {
    getLatestBlogs,
    getLatestArticles
}

export default exploreService;