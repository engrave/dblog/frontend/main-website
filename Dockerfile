FROM node:10-alpine as development

WORKDIR /app

COPY --chown=node package*.json ./
COPY --chown=node tsconfig.json ./
COPY --chown=node healthcheck.js /healthcheck.js

RUN npm install

COPY --chown=node src src
COPY --chown=node public public
COPY --chown=node views views

RUN npm run build

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait

ENV FRONT=engrave

# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static /tini
RUN chmod +x /tini

ENTRYPOINT ["/tini", "--"]

## Launch the wait tool and then your application
CMD /wait && npm run watch

########################################################################################

FROM node:alpine as production

RUN mkdir -p /app && chown -R node:node /app

USER node

WORKDIR /app

COPY --chown=node --from=development /app/package*.json ./
COPY --chown=node --from=development /app/dist dist
COPY --chown=node --from=development /app/public public
COPY --chown=node --from=development /app/views views
COPY --chown=node --from=development /wait /wait
COPY --chown=node --from=development /healthcheck.js /healthcheck.js
COPY --chown=node --from=development /tini /tini

RUN npm ci --production

ENV FRONT=engrave

HEALTHCHECK --interval=60s --timeout=5s --start-period=60s --retries=5 CMD node /healthcheck.js

ENTRYPOINT ["/tini", "--"]

## Launch the wait tool and then your application
CMD /wait && npm run start